﻿using Microsoft.AspNetCore.Mvc;
using Tesseract;
using test123.Dto;


namespace test123.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet(Name = "GetWeatherForecast")]
        public IEnumerable<WeatherForecast> Get()
        {
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateOnly.FromDateTime(DateTime.Now.AddDays(index)),
                TemperatureC = Random.Shared.Next(-20, 55),
                Summary = Summaries[Random.Shared.Next(Summaries.Length)]
            })
            .ToArray();
        }

        [HttpPost("getCaptcha")]
        public string getCaptcha(ImageDto imageDto)
        {
            using (var engine = new TesseractEngine(@"./tessdata", "train", EngineMode.Default))
            {
                using (var img = Pix.LoadFromMemory(ConvertIFormFileToByteArray(imageDto.Image)))
                {
                    using (var page = engine.Process(img))
                    {
                        string text = page.GetText();

                        return text;
                    }
                }
            }
        }

        public static byte[] ConvertIFormFileToByteArray(IFormFile file)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                file.CopyTo(memoryStream);
                return memoryStream.ToArray();
            }
        }
    }
}
